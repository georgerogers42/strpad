extern crate strpad;

#[test]
fn test_overlen() {
    let s = strpad::pad_left(120, "");
    assert_eq!(120, strpad::pad_left(100, &s).chars().count());
    assert_eq!(120, strpad::pad_right(100, &s).chars().count());
    assert_eq!(s, strpad::pad_left(100, &s));
    assert_eq!(s, strpad::pad_right(100, &s));
}
