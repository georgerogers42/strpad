fn pad_width(a: usize, b: usize) -> usize {
    if a > b { a - b } else { 0 }
}

pub fn pad_left_with(c: char, n: usize, s: &str) -> String {
    let mut res = c.to_string().repeat(pad_width(n, s.chars().count()));
    res.push_str(s);
    res
}

pub fn pad_left(n: usize, s: &str) -> String {
    pad_left_with(' ', n, s)
}

pub fn pad_right_with(c: char, n: usize, s: &str) -> String {
    let mut res = s.to_owned();
    res.push_str(&c.to_string().repeat(pad_width(n, s.chars().count())));
    res
}

pub fn pad_right(n: usize, s: &str) -> String {
    pad_right_with(' ', n, s)
}
